﻿Path_srcdir = replace(Environment.Value("TestDir"), Environment.Value("TestName"),"") 'Getting src dir path
Environment.LoadFromFile Path_srcdir+"TestInputs\Configs.xml"	'Loading environment values
LoadFunctionLibrary Path_srcdir+"FunctionLibrary\ApplicationMethods.qfl"	'Associating functional libraries
RepositoriesCollection.Add(Path_srcdir+"ObjectRepository\WebShopOR.tsr")	'Associating shared OR
print Environment.Value("Path_browserEXE")
Setting("DisableReplayUsingAlgorithm") = 1
DataTable.ImportSheet Path_srcdir&"TestInputs\TCInput.xlsx", "TCDEMO", "Global"
DataTable.GetSheet "Global"
DataTable.SetCurrentRow 1
str_category = DataTable.Value("Category","Global")
str_Item = DataTable.Value("Item","Global")
str_User = DataTable.Value("UserID","Global")
str_Password = DataTable.Value("Password","Global")
int_qty = cint(DataTable.Value("Quantity","Global"))
str_Address = DataTable.Value("Address","Global")
str_City = DataTable.Value("City","Global")
str_Email = DataTable.Value("Email","Global")
str_FirstName = DataTable.Value("FirstName","Global")
str_LastName = DataTable.Value("LastName","Global")
str_PhoneNumber = DataTable.Value("PhoneNumber","Global")
str_PostalCode = DataTable.Value("PostalCode","Global")
str_Country = DataTable.Value("Country","Global")

Call LaunchWebShop
Call LogintoWebShop(str_User, str_Password)
Call AddItemstoCartAndCheckOut(str_category, str_Item, int_qty)
Call AddBillingAndShippingAddresses(str_FirstName, str_LastName, str_PhoneNumber, str_Email, str_Address, str_City, str_Country, str_PostalCode)
Call CompleteShipment_PaymentMethods_AndConfirmOrder()


